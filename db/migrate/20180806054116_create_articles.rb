class CreateArticles < ActiveRecord::Migration[5.2]
  def change
    create_table :articles do |t|
      t.string :nama
      t.string :email
      t.string :no_telp
      t.text :alamat

      t.timestamps
    end
  end
end
