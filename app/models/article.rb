class Article < ApplicationRecord
	has_many :comments, dependent: :destroy
	validates :nama, presence: true, 
	length: { minimum: 5 }

end
